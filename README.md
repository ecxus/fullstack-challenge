# Desafio Desenvolvedor Fullstack

## Sobre o desafio

O desafio é criar uma API e uma interface que permita criar, consultar, atualizar e deletar apartamentos.

### Tecnologias

- Java;
- Spring Boot;
- Hibernate;
- PostgreSQL;
- Frontend: Tecnologia de sua preferência (e.g. React, AngularJS, Vue.js).

### Requisitos

A entidade `Apartamento` deve conter os atributos especificados na tabela abaixo:

| Atributo | Descrição                      |
|----------|--------------------------------|
| `id`     | Identificador único da tabela. |
| `numero` | Número do apartamento.         |
| `estado` | Estado do apartamento.         |

O número e o estado do apartamento são obrigatórios. O número deve ser único e o estado é limitado aos seguintes
valores: **LIVRE** e **LOCADO**.

A API deve conter as seguintes rotas:

| Método   | Endpoint           | Descrição                                                   |
|----------|--------------------|-------------------------------------------------------------|
| `GET`    | `/api/apartamento` | Retorna todos os apartamentos.                              |
| `POST`   | `/api/apartamento` | Cria um apartamento com número e o estado LIVRE por padrão. |
| `PUT`    | `/api/produto/:id` | Atualiza o número e o estado de um apartamento.             |
| `DELETE` | `/api/produto/:id` | Deleta um apartamento.                                      |

Você é livre para adicionar outras rotas que achar pertinente. Adicione as validações necessárias de acordo com cada
rota.

### Critérios para avaliação

- Cumprimento dos requisitos;
- Simplicidade e clareza da solução;
- Qualidade e estilo de código;
- Arquitetura do projeto;
- Documentação e organização geral do projeto.

### Entrega

Por favor nos envie o link do repositório no e-mail <a href="mailto:desenvolvimento@ecxus.com.br">
desenvolvimento@ecxus.com.br</a>. Vamos fazer nosso melhor para analisar seu projeto no menor tempo hábil.
